function FindProxyForURL(url, host)
{
	if (isInNet(host, "10.1.0.0", "255.255.0.0"))
	{
		return "DIRECT";
	}
	else
	{
		return "PROXY squid1.ad.rtps.link:3128; DIRECT";
	}
}